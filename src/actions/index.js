import axios from 'axios';

export const FETCH_PLACES = 'FETCH_PLACES';
export const FETCH_PLACE = 'FETCH_PLACE';
export const CREATE_PLACE = 'CREATE_PLACE';
export const DELETE_PLACE = 'DELETE_PLACE';
export const EDIT_PLACE = 'EDIT_PLACE';

const ROOT_URL = 'http://localhost:3000/';

export function fetchPlaces() {
  const request = axios.get(`${ROOT_URL}places`);

  return {
    type: FETCH_PLACES,
    payload: request
  };
}

export function fetchPlace(id) {
  const request = axios.get(`${ROOT_URL}places/${id}`);

  return {
    type: FETCH_PLACE,
    payload: request
  };
}

export function createPlace(props) {
  const request = axios.post(`${ROOT_URL}places`, props);

  return {
    type: CREATE_PLACE,
    payload: request
  };
}

export function deletePlace(id) {
  const request = axios.delete(`${ROOT_URL}places/${id}`);

  return {
    type: DELETE_PLACE,
    payload: request
  };
}

export function editPlace(id, props) {
  const request = axios.put(`${ROOT_URL}places/${id}`, props);

  return {
    type: EDIT_PLACE,
    payload: request
  };
}