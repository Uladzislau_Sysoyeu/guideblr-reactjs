import { FETCH_PLACES, FETCH_PLACE } from '../actions/index';

const INITIAL_STATE = { all: [], place: null};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {

  	case FETCH_PLACE:
      return {...state, place: action.payload.data.sight};

    case FETCH_PLACES:
      return { ...state, all: action.payload.data.sights };

    default:
      return state;
  }
}