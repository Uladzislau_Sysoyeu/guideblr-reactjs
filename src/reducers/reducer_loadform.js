// // Quack! This is a duck. https://github.com/erikras/ducks-modular-redux
// const LOAD = 'redux-form-examples/account/LOAD'

// const reducer = (state = {}, action) => {
//   switch (action.type) {
//     case LOAD:
//       return {
//         data: action.data
//       }
//     default:
//       return state
//   }
// }

// /**
//  * Simulates data loaded into this reducer from somewhere
//  */
// export const load = data => ({ type: LOAD, data });

// export default reducer;

import { EDIT_PLACE } from '../actions/index';

const INITIAL_STATE = {place: null};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case EDIT_PLACE:
      return {...state, place: action.payload.data.sight}
    default:
      return state
  }
}
