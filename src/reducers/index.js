import { combineReducers } from 'redux';
import PlacesReducer from './reducer_place';
import LoadFormReducer from './reducer_loadform';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  places: PlacesReducer,
  form: formReducer,
  loadform: LoadFormReducer
});

export default rootReducer;
