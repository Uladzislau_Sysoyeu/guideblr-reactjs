import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app';
import SimpleMap from './components/googleMap';
import TitlePage from './components/titlePage';
import PlacesShow from './components/placesShow';
import PlacesNew from './components/placesNew';
import PlacesEdit from './components/PlacesEdit';

export default (
  <Route path='/' component={App}>
    <IndexRoute component={TitlePage} />
    <Route path='places' component={SimpleMap} />
    <Route path='places/new' component={PlacesNew}/>
    <Route path='places/:id' component={PlacesShow}/>
    <Route path='places/:id/edit' component={PlacesEdit}/>
  </Route>
);