import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class TitlePage extends Component {
  
  renderPlace() {
    return (
      <div>
      <li className='list-group-item btn' >
        <Link to='/places'>
        Show map
        </Link>
      </li>

      <li className='list-group-item btn' >
        <Link to='places/new'>
            Create place
        </Link>
      </li>
      </div>
    );
  }
  
  render() {
    return (
      <div className='wrappers'>
        {this.renderPlace()}
      </div>
    );
  }
}

export default connect (null)(TitlePage);