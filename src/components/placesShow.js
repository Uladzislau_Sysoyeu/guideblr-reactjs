import React from 'react';
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchPlace, deletePlace } from '../actions/index';
import { Link, browserHistory } from 'react-router'; 

class PlacesShow extends Component {

  static contextTypes = {
    router: PropTypes.object
  };
  
  componentWillMount() {
    this.onEditClick = this.onEditClick.bind(this);
    this.props.fetchPlace(this.props.params.id);
  }
  
  onDeleteClick() {
    this.props.deletePlace(this.props.params.id)
      .then(() => { this.context.router.push('/') });
  }
  
   onEditClick(place) {
    browserHistory.push('places/' + place._id + '/edit'); 
  }

  render() {
    const { place } = this.props;
    
    if (!this.props.place) {
      return (
        <div>Loading...</div>
      );
    }
    
    return (
      <div>
       <Link to='places'>Back to Map</Link>
       <button 
         className='btn btn-danger pull-xs-right' 
         onClick={this.onDeleteClick.bind(this)}>Delete Place</button>
       <button 
         className='btn btn-danger pull-xs-right' 
         onClick={() => this.onEditClick(place)}>Edit Place</button> 
        <h3 className='content'>{place.title}</h3>
        <h6 className='content'>Description: {place.description}</h6>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { place: state.places.place };
}

export default connect(mapStateToProps, { fetchPlace, deletePlace })(PlacesShow);