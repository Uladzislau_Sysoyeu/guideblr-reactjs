// import React from 'react';
// import {Component} from 'react';
// import { connect } from 'react-redux';
// import { Field, reduxForm } from 'redux-form';
// import { load as loadAccount } from '../reducers/reducer_loadform';
// const data = {  // used to populate "account" reducer when "Load" is clicked
//   firstName: 'Jane',
//   lastName: 'Doe',
//   age: '42',
//   sex: 'female',
//   employed: true,
//   favoriteColor: 'Blue',
//   bio: 'Born to write amazing Redux code.'
// }
// const colors = [ 'Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Indigo', 'Violet' ]

// let InitializeFromStateForm = props => {
//   const { handleSubmit, load, pristine, reset, submitting } = props
//   return (
//     <form onSubmit={handleSubmit}>
//       <div>
//         <button type="button" onClick={() => load(data)}>Load Account</button>
//       </div>
//       <div>
//         <label>First Name</label>
//         <div>
//           <Field name="firstName" component="input" type="text" placeholder="First Name"/>
//         </div>
//       </div>
//       <div>
//         <label>Last Name</label>
//         <div>
//           <Field name="lastName" component="input" type="text" placeholder="Last Name"/>
//         </div>
//       </div>
//       <div>
//         <label>Age</label>
//         <div>
//           <Field name="age" component="input" type="number" placeholder="Age"/>
//         </div>
//       </div>
//       <div>
//         <label>Sex</label>
//         <div>
//           <label><Field name="sex" component="input" type="radio" value="male"/> Male</label>
//           <label><Field name="sex" component="input" type="radio" value="female"/> Female</label>
//         </div>
//       </div>
//       <div>
//         <label>Favorite Color</label>
//         <div>
//           <Field name="favoriteColor" component="select">
//             <option value="">Select a color...</option>
//             {colors.map(colorOption =>
//               <option value={colorOption} key={colorOption}>{colorOption}</option>)}
//           </Field>
//         </div>
//       </div>
//       <div>
//         <label htmlFor="employed">Employed</label>
//         <div>
//           <Field name="employed" id="employed" component="input" type="checkbox"/>
//         </div>
//       </div>
//       <div>
//         <label>Bio</label>
//         <div>
//           <Field name="bio" component="textarea"/>
//         </div>
//       </div>
//       <div>
//         <button type="submit" disabled={pristine || submitting}>Submit</button>
//         <button type="button" disabled={pristine || submitting} onClick={reset}>Undo Changes</button>
//       </div>
//     </form>
//   )
// }

// // Decorate with reduxForm(). It will read the initialValues prop provided by connect()
// InitializeFromStateForm = reduxForm({
//   form: 'initializeFromState',
//   fields: ['firstName', 'lastName', 'age', 'sex', 'favoriteColor', 'employed', 'bio']  // a unique identifier for this form
// })(InitializeFromStateForm)

// // You have to connect() to any reducers that you wish to connect to yourself
// InitializeFromStateForm = connect(
//   state => ({
//     initialValues: state.places.place // pull initial values from account reducer
//   }),
//   { load: loadAccount }               // bind account loading action creator
// )(InitializeFromStateForm)

// export default InitializeFromStateForm;

import React, {Component, PropTypes} from 'react';
import { reduxForm } from 'redux-form';
import { fetchPlace, editPlace } from '../actions/index';
import { Link } from 'react-router';
import { connect } from 'react-redux';


class PlacesEdit extends Component {
  
  static contextTypes = {
    router: PropTypes.object 
  };

  componentDidMount() {
    this.onFormSubmit = this.onFormSubmit.bind(this);
    //this.props.fetchPlace(this.props.params.id);
  }
  
  onFormSubmit(place, props) {
    this.props.editPlace(this.props.params.id, props)
      .then(() => {
        this.context.router.push('/');
        console.log(this.props.params.id);
      });
  }
  
  render() {
    const { place } = this.props;
    const handleSubmit = this.props;
    return (
      <form onSubmit={ () => { handleSubmit(this.onFormSubmit(place))  } } className='forms'>
        <h3>Edit place</h3>
        
        <div className={`form-group`}>
          <input type='text' className='form-control' defaultValue={place.lat} placeholder='Enter a latitude' />
        </div>

        <div className={`form-group`}>
          <input type='text' className='form-control' defaultValue={place.lng} placeholder='Enter a Longitude' />
        </div>

        <div className={`form-group`}>
          <input type='text' className='form-control' defaultValue={place.title} placeholder='Enter a title'  />
        </div>

        <div className={`form-group`}>
          <input type='text' className='form-control' defaultValue={place.description} placeholder='Enter description' />
        </div>

        <div className={`form-group`}>
          <input type='text' className='form-control' defaultValue={place.picture} placeholder='Enter a picture' />
        </div>
        
        <button type='submit' className='btn btn-primary buttons'>Submit</button>
        <Link to='/' className='btn btn-danger buttons'>Cancel</Link>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return { place: state.places.place };
}

PlacesEdit = reduxForm({
  form: 'PlacesEditForm',
  fields: ['lat', 'lng', 'title', 'description', 'picture'],
})(PlacesEdit);

PlacesEdit = connect(mapStateToProps, {fetchPlace, editPlace})(PlacesEdit);

export default PlacesEdit;
