import React, {Component} from 'react';
import { connect } from 'react-redux';
import {GoogleMapLoader, GoogleMap, Marker} from 'react-google-maps';
import { fetchPlaces } from '../actions/index';
import { browserHistory } from 'react-router';

class SimpleMap extends Component {

  componentDidMount() {
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.props.fetchPlaces();
  }

  handleMarkerClick(place) {
    browserHistory.push('places/' + place._id);
  }

  renderPlaces() {
    
      return (
        <section style={{height: "100%"}}>
          <GoogleMapLoader 
            containerElement={ <div {...this.props.containerElementProps} style={{ height: "100%", }} /> }
            googleMapElement={ <GoogleMap defaultZoom={7} defaultCenter={{ lat: 53.917404, lng: 27.584369 }} >
                {this.props.places.map((place) => {
                  return  <Marker 
                  position={{ lat: +place.lat, lng: +place.lng }} 
                  icon={place.picture} 
                  onClick={() => this.handleMarkerClick(place)}
                  name={place.title} 
                  key={place.title}/> } )
                }
              </GoogleMap> }
            />
        </section>
      );
//    });
  }

  render() {
    return (
      <div style={{ height: "100%", }}>
        {this.renderPlaces()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { places: state.places.all };
}

export default connect (mapStateToProps, { fetchPlaces })(SimpleMap);