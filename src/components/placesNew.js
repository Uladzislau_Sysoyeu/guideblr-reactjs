import React, {Component, PropTypes} from 'react';
import { reduxForm } from 'redux-form';
import { createPlace } from '../actions/index';
import { Link } from 'react-router';


class PlacesNew extends Component {
  
  static contextTypes = {
    router: PropTypes.object 
  };
  
  onSubmit(props) {
    this.props.createPlace(props)
      .then(() => {
        this.context.router.push('/');
      });
  }
  
  render() {
    const { fields: { lat, lng, title, description, picture }, handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className='forms'>
        <h3>Create a new place</h3>
        
        <div className={`form-group ${lat.touched && lat.invalid ? 'has-danger' : ''}`}>
          <input type='text' className='form-control' placeholder='Enter a latitude' {...lat} />
          <div className='text-help'>
            {lat.touched ? lat.error : ''}
          </div>
        </div>
        <div className={`form-group ${lng.touched && lng.invalid ? 'has-danger' : ''}`}>
          <input type='text' className='form-control' placeholder='Enter a Longitude' {...lng} />
          <div className='text-help'>
            {lng.touched ? lng.error : ''}
          </div>
        </div>
        <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
          <input type='text' className='form-control' placeholder='Enter a title' {...title} />
          <div className='text-help'>
            {title.touched ? title.error : ''}
          </div>
        </div>
        <div className={`form-group ${description.touched && description.invalid ? 'has-danger' : ''}`}>
          <input type='text' className='form-control' placeholder='Enter description' {...description} />
          <div className='text-help'>
            {description.touched ? description.error : ''}
          </div>
        </div>
        <div className={`form-group ${picture.touched && picture.invalid ? 'has-danger' : ''}`}>
          <input type='text' className='form-control' placeholder='Enter a picture' {...picture} />
          <div className='text-help'>
            {picture.touched ? picture.error : ''}
          </div>
        </div>
        
        <button type='submit' className='btn btn-primary buttons'>Submit</button>
        <Link to='/' className='btn btn-danger buttons'>Cancel</Link>
      </form>
    );
  }
}

function validate(values) {
  const errors = {};
  if(!values.lat) {
    errors.lat = 'Latitude is required';
  }
  
  if(!values.lng) {
    errors.lng = 'Longitude is required';
  }
  
  if(!values.title) {
    errors.title = 'Title is required';
  }

  if(!values.description) {
    errors.description = 'Description is required';
  }

  if(!values.picture) {
    errors.picture = 'Picture is required';
  }
  return errors;
}

export default reduxForm({
  form: 'PlacesNewForm',
  fields: ['lat', 'lng', 'title', 'description', 'picture'],
  validate
}, null, { createPlace })(PlacesNew);